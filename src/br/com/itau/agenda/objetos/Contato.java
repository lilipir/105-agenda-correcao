package br.com.itau.agenda.objetos;

public class Contato {
	
	private String nome;
	private long telefone;
	
	public Contato(String nome, long telefone) {
		this.nome = nome;
		this.telefone = telefone;
	}
	
	@Override
	public String toString() {
		return nome + " = " + telefone;
	}

}
