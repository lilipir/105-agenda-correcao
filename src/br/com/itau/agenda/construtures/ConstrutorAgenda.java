package br.com.itau.agenda.construtures;

import br.com.itau.agenda.console.Console;
import br.com.itau.agenda.objetos.Agenda;

public class ConstrutorAgenda {
	
	public static Agenda criar() {
		Console.imprimeSaudacao();
		
		String nome = Console.lerString();
		
		Agenda agenda = new Agenda(nome);
		
		return agenda;
	}

}
